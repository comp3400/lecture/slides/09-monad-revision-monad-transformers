## Revision { data-transition="fade none" }

###### The Monad type class

* `Monad` is a subclass of `Applicative` is a subclass of `Functor`
* The `Monad` type class gets special syntax treatment in Haskell (`do` notation)

<pre><code class="language-haskell hljs" data-trim data-noescape>
Functor (&lt;$>)     ::   (a -> b) -> k a -> k b
Applicative (&lt;*>) :: k (a -> b) -> k a -> k b
Monad (=&lt;&lt;)       :: (a -> k b) -> k a -> k b
Monad (>>=)       :: k a -> (a -> k b) -> k b
Applicative pure  :: a -> k a
</code></pre>

<aside class="notes">
  <ul>
    <li><code>(&lt;$>)</code> pronounced "effmap"</li>
    <li><code>(&lt;*>)</code> pronounced "apply"</li>
    <li><code>(=&lt;&lt;)</code> pronounced "bind"</li>
  </ul>
</aside>

---

###### The Monad type class

* `return` is an historical artifact that is otherwise on the `Applicative` super class
* `(>>=)` is `(=<<)` with the arguments flipped, for convenience reasons

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Applicative k => Monad k where
  (>>=) :: k a -> (a -> k b) -> k b
  return :: a -> k a
</code></pre>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f >>= \ff ->
  a >>= \aa ->
  pure (ff aa)
</code></pre>

<pre><code class="language-haskell hljs" data-noescape>(>>=) :: k x -> (x -> k y) -> k y





</code></pre>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
\<mark>f</mark> a ->
  <mark>f</mark> >>= \ff ->
  a >>= \aa ->
  pure (ff aa)
</code></pre>

<pre><code class="language-haskell hljs" data-noescape>(>>=) :: <mark>k x</mark> -> (x -> k y) -> k y
f     :: <mark>k x</mark>




</code></pre>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f <mark>>>=</mark> \<mark>ff</mark> ->
  a >>= \aa ->
  pure (<mark>ff</mark> aa)
</code></pre>

<pre><code class="language-haskell hljs" data-noescape><mark>(>>=)</mark> :: k (p -> q) -> (<mark>(p -> q)</mark> -> k y) -> k y
f     :: k (<mark>p -> q</mark>)
ff    :: <mark>p -> q</mark>



</code></pre>

<aside class="notes">
  <ul>
    <li>The type of <code>f</code> relates to the type of <code>ff</code></li>
    <li><code>ff</code> is clearly a function, as it is applied in the last line</li>
    <li>Therefore the type of <code>f</code> is "a k of some function"</li>
  </ul>
</aside>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f <mark>>>=</mark> \ff ->
  a >>= \aa ->
  pure (<mark>ff aa</mark>)
</code></pre>

<pre><code class="language-haskell hljs" data-noescape><mark>(>>=)</mark> :: k (p -> <mark>q</mark>) -> ((p -> <mark>q</mark>) -> k <mark>y</mark>) -> k <mark>y</mark>
f     :: k (p -> q)
ff    :: p -> q



</code></pre>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f <mark>>>=</mark> \ff ->
  a >>= \aa ->
  pure (<mark>ff aa</mark>)
</code></pre>

<pre><code class="language-haskell hljs" data-noescape><mark>(>>=)</mark> :: k (p -> q) -> ((p -> q) -> k <mark>q</mark>) -> k <mark>q</mark>
f     :: k (p -> q)
ff    :: p -> q



</code></pre>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f >>= \ff ->
  a >>= \aa ->
  <mark>pure (ff aa)</mark>
</code></pre>

<pre><code class="language-haskell hljs" data-noescape>(>>=) :: k (p -> q) -> ((p -> q) -> <mark>k q</mark>) -> <mark>k q</mark>
f     :: k (p -> q)
ff    :: p -> q



</code></pre>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f >>= \ff ->
  <mark>a</mark> >>= \aa ->
  pure (ff aa)
</code></pre>

<pre><code class="language-haskell hljs" data-noescape>(>>=) :: k (p -> q) -> ((p -> q) -> k q) -> k q
f     :: k (p -> q)
ff    :: p -> q
a     :: <mark>k p</mark>


</code></pre>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f >>= \ff ->
  a >>= \<mark>aa</mark> ->
  pure (ff <mark>aa</mark>)
</code></pre>

<pre><code class="language-haskell hljs" data-noescape>(>>=) :: k (p -> q) -> ((p -> q) -> k q) -> k q
f     :: k (p -> q)
ff    :: p -> q
a     :: k p
aa    :: <mark>p</mark>

</code></pre>

---

## Why is `Monad` a subclass of `Applicative`?

<div style="text-align: center; font-size: 24px">
  <code>(&lt;*>)</code> can be recovered with `(>>=)` and `pure`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>\f a ->
  f >>= \ff ->
  a >>= \aa ->
  pure (ff aa)</mark>
</code></pre>

<pre><code class="language-haskell hljs" data-noescape>(>>=) :: k (p -> q) -> ((p -> q) -> k q) -> k q
f     :: k (p -> q)
ff    :: p -> q
a     :: k p
aa    :: p
_     :: <mark>k (p -> q) -> k p -> k q</mark>
</code></pre>

---

## `do` notation { data-transition="fade none" }

---

## `do` notation

* `do` is a Haskell keyword
* `do` notation is a syntactic conversion to/from `(>>=)`
* `do` notation works for *any monad*
* Other programming languages have similar notation
  * C# LINQ
  * Scala for comprehensions

