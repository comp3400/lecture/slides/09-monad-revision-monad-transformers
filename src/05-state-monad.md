## State { data-transition="fade none" }

###### FAQ

* Q: Can I set just one variable and update it later?
* A: No, this is Functional Programming.
* Q: But...
* A: State, you want State.
* Q: What is State?

---

## State

###### A simple use-case

* Suppose we wish to add `10` to every element in a list
* This is trivial using `fmap`
* `add10 = fmap (+10)`

---

## State

###### A simple use-case

* Now suppose we also wanted to count the how many `3` that we saw in the list
* We want to do this **while adding 10 to each list element**
* It is tempting to reach for a variable *(and find that you cannot anyway)*
* This variable would keep a count when we see `3` while we also `fmap` through the list

---

## State

###### A depiction of this temptation &#x1F608;

<pre><code class="language-haskell hljs" data-trim data-noescape>
add10 = {
  var counter = 0
  let x =
        fmap (\n -> 
          if n == 3
            then counter++
          n + 10
        )
  return (x, counter)
}
</code></pre>

---

## State

###### A depiction of this temptation &#x1F608;

<pre><code class="language-haskell hljs" data-trim data-noescape>
add10 = {
  var <mark>counter</mark> = <mark>0</mark>
  let x =
        fmap (\n -> 
          if n == 3
            then <mark>counter</mark>++
          n + 10
        )
  return (x, <mark>counter</mark>)
}
</code></pre>

---

## State

###### A depiction of this temptation &#x1F608;

<pre><code class="language-haskell hljs" data-trim data-noescape>
add10 = {
  var <mark>counter</mark> = <mark>0</mark>
  let x =
        fmap (\n -> 
          if n == 3
            then <mark>0</mark>++
          n + 10
        )
  return (x, <mark>0</mark>)
}
</code></pre>

---

## State

###### A simple use-case

* What we *actually* want is a function that, not only "adds 10", but also **reads** the current counter
* and produces a new counter value on each visit to a list element
* Consider:
  * fmap :: (a -> b) -> List a -> List b
  * We want to add in a function to this `:: Int -> Int`

---

## State

###### A simple use-case

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap         ::
  (a ->              b) ->  List a ->              List b
fmapIntState ::
  (a -> <mark>Int -> (Int,</mark> b)) -> List a -> <mark>Int -> (Int,</mark> List b)
</code></pre>

---

## State

###### A simple use-case

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmapIntState ::
  (a -> Int -> (Int, b)) -> List a -> Int -> (Int, List b)

add10 list =
  let is3 x =
        if x == 3 then (+1) else id
  in  fmapIntState (\a n -> (is3 a n, a + 10)) list 0
</code></pre>

---

## State

###### A simple use-case

* There is a neater and more general way to structure this code
* Let's start with a data type that denotes a *state transition*

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s a =
  State (s -> (s, a))
</code></pre>

* The `(s)` value is the state that transitions
* The `(a)` value is produced through the state transition

---

## State

###### A simple use-case

* We could rewrite `fmapIntState` to use our data structure
* All we have done is wrapped the function `:: Int -> (Int, _)` as a data type

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s a =
  State (s -> (s, a))

-- was
fmapIntState ::
  (a -> Int -> (Int, b)) -> List a -> Int -> (Int, List b)
-- now
fmapIntState ::
  (a -> State Int b) -> List a -> State Int (List b)
</code></pre>

---

## State

###### A simple use-case

* Let's look closer at the `State` data type
* `> :kind State`
* `> :kind State Int`
* `State Int` has the correct kind for a `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s a =
  State (s -> (s, a))
</code></pre>

---

## State

###### `State s` is a functor!

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s a =
  State (s -> (s, a))

instance Functor (State s) where
  fmap f (State g) =
    State (\s -> 
      let (s', a) = g s
      in  (s', f a)
    )
</code></pre>

---

## State

###### `Applicative` ?

---

## State

###### `State s` is an applicative functor!

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s a =
  State (s -> (s, a))

instance Applicative (State s) where
  pure a =
    State (\s -> (s, a))
  State f &lt;*> State a =
    State (\s ->
      let (t, f') = f s
          (u, a') = a t
      in  (u, f' a')
    )
</code></pre>

---

## State

###### `Monad` ?

---

## State

###### `State s` is a monad!

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s a =
  State (s -> (s, a))

instance Monad (State s) where
  -- history
  return =
    pure
  State q >>= f =
    State (\s ->
      let (t, a) = q s
          State r = f a
      in  r t
    )
</code></pre>

---

## State is a Monad

###### so what ?

---

## State

###### Let's write `fmapIntState`

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmapIntState ::
  (a -> State Int b) -> List a -> State Int (List b)
fmapIntState _ Nil =
  pure Nil
fmapIntState f (Cons h t) =
  f h >>= \h' ->
  fmapIntState f t >>= \t' ->
  pure (Cons h' t')
</code></pre>

---

## State

###### Let's write `fmapIntState`

* We have only used Monad operations
* Nothing to do with `State` actually

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmapIntState ::
  (a -> <mark>State Int</mark> b) -> List a -> <mark>State Int</mark> (List b)

fmapIntState _ Nil =
  <mark>pure</mark> Nil
fmapIntState f (Cons h t) =
  f h <mark>>>=</mark> \h' ->
  fmapIntState f t <mark>>>=</mark> \t' ->
  <mark>pure</mark> (Cons h' t')
</code></pre>

---

## State

###### Let's generalise `fmapIntState`

* We have only used Monad operations
* Nothing to do with `State` actually

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmapIntState ::
  Monad k =>
  (a -> <mark>k</mark> b) -> List a -> <mark>k</mark> (List b)
fmapIntState _ Nil =
  <mark>pure</mark> Nil
fmapIntState f (Cons h t) =
  f h <mark>>>=</mark> \h' ->
  fmapIntState f t <mark>>>=</mark> \t' ->
  <mark>pure</mark> (Cons h' t')
</code></pre>

---

## State

###### generalise `fmapIntState`

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmapIntState ::
  Monad k =>
  (a -> k b) -> List a -> k (List b)
fmapIntState _ Nil =
  pure Nil
fmapIntState f (Cons h t) =
  f h >>= \h' ->
  fmapIntState f t >>= \t' ->
  pure (Cons h' t')
</code></pre>

---

## State

###### generalise `fmapIntState`

* We might even consider renaming it
* After all, there is no mention of `State`

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>fmapIntState</mark> ::
  Monad k =>
  (a -> k b) -> List a -> k (List b)
<mark>fmapIntState</mark> _ Nil =
  pure Nil
<mark>fmapIntState</mark> f (Cons h t) =
  f h >>= \h' ->
  <mark>fmapIntState</mark> f t >>= \t' ->
  pure (Cons h' t')
</code></pre>

---

## State

###### rename generalised `fmapIntState`

<pre><code class="language-haskell hljs" data-trim data-noescape>
traverse ::
  Monad k =>
  (a -> k b) -> List a -> k (List b)
traverse _ Nil =
  pure Nil
traverse f (Cons h t) =
  f h >>= \h' ->
  traverse f t >>= \t' ->
  pure (Cons h' t')
</code></pre>

---

## State

<div style="text-align: center; font-size: 36px">
  Who remembers week 3 prac?
</div>

---

## State

<div style="text-align: center; font-size: 36px">
  Who remembers week 3 prac?
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
traverseListOptional ::
  (a -> Optional b) -> List a -> Optional (List b)
</code></pre>

---

## State

<pre><code class="language-haskell hljs" data-trim data-noescape>
traverseListOptional ::
  (a -> Optional b) -> List a -> Optional (List b)
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
traverse ::
  Monad k => (a -> k b) -> List a -> k (List b)
</code></pre>

---

## State

<pre><code class="language-haskell hljs" data-trim data-noescape>
           (a -> Optional b) -> List a -> Optional (List b)
Monad k => (a -> k        b) -> List a -> k        (List b)
</code></pre>

---

## State

###### Back to the use-case

* add `10` to each list element, and count all the `3` values
* We have `traverse`
  * `traverse :: Monad k => (a -> k b) -> List a -> k (List b)`
* `State Int` is a `Monad`

---

## State

###### Back to the use-case

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s = State (s -> (s, a))

-- get the function out of the constructor
runState :: State s a -> s -> (s, a)
runState (State k) = k

instance Functor (State s) where -- todo
instance Applicative (State s) where -- todo
instance Monad (State s) where -- todo
</code></pre>

---

## State

###### Back to the use-case

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s = State (s -> (s, a))

-- get the function out of the constructor
runState :: State s a -> s -> (s, a)
runState (State k) = k

instance Functor (State s) where -- todo
instance Applicative (State s) where -- todo
instance Monad (State s) where -- todo

add10 :: List Int -> (Int, List Int)
add10 list =
  let is3 x =
        if x == 3 then (+1) else id
      count =
        traverse (\a -> State (\n -> (is3 a n, a + 10))) list
  in  runState count 0
</code></pre>

---

## State

* `State` is one way to simulate *passing a variable around in a loop*
* `traverse` is a generalised `fmap` that runs through any `Monad`
* We will look further at `traverse` through the semester

---

## State (full working example)

<div style="text-align: center; font-size: 14px">
<pre><code class="language-haskell hljs" data-trim data-noescape>
-- hide the real traverse, for now
import Prelude hiding (traverse)

data State s a =
  State (s -> (s, a))

runState :: State s a -> s -> (s, a)
runState (State k) = k

instance Functor (State s) where
  fmap f (State g) =
    State (\s -> 
      let (s', a) = g s
      in  (s', f a)
    )

instance Applicative (State s) where
  pure a =
    State (\s -> (s, a))
  State f &lt;*> State a =
    State (\s ->
      let (t, f') = f s
          (u, a') = a t
      in  (u, f' a')
    )

instance Monad (State s) where
  -- history
  return =
    pure
  State q >>= f =
    State (\s ->
      let (t, a) = q s
          State r = f a
      in  r t
    )

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)

traverse ::
  Monad k =>
  (a -> k b) -> List a -> k (List b)
traverse _ Nil =
  pure Nil
traverse f (Cons h t) =
  f h >>= \h' ->
  traverse f t >>= \t' ->
  pure (Cons h' t')

add10 :: List Int -> (Int, List Int)
add10 list =
  let is3 x =
        if x == 3 then (+1) else id
      count =
        traverse (\a -> State (\n -> (is3 a n, a + 10))) list
  in  runState count 0

test =
  let (threes, list) = add10 (Cons 1 (Cons 2 (Cons 3 (Cons 4 (Cons 3 (Cons 5 Nil))))))
  in  do  putStrLn ("There are " ++ show threes ++ " threes")
          print list
</code></pre>
</div>
