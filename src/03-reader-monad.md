## The Reader monad { data-transition="fade none" }

###### An example

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

-- remove the function from the constructor
runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f
</code></pre>

* This data type is an instance of `Monad`
* Therefore, also `Functor` and `Applicative`

---

## The Reader monad

###### Let's witness it

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  -- Found hole: _ :: (a -> b) -> IntReader a -> IntReader b
  fmap = _
</code></pre>

---

## The Reader monad

###### Let's witness it

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  -- Found hole: _ :: (a -> b) -> IntReader a -> IntReader b
  fmap =
    \func (IntReader int2a) ->
      IntReader (\i -> func (int2a i))
</code></pre>

---

## The Reader monad

###### Let's witness it

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  -- Found hole: _ :: (a -> b) -> IntReader a -> IntReader b
  fmap =
    \func (IntReader int2a) ->
      IntReader (<mark>\i -> func (int2a i)</mark>)
</code></pre>

---

## The Reader monad

###### Let's witness it

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  -- Found hole: _ :: (a -> b) -> IntReader a -> IntReader b
  fmap =
    \func (IntReader int2a) ->
      IntReader (func . int2a)
</code></pre>

---

## The Reader monad

###### Let's witness it

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  fmap =
    \func (IntReader int2a) ->
      IntReader (func . int2a)

instance Applicative IntReader where
  pure = _pure
  (&lt;*>) = _apply
</code></pre>

---

## The Reader monad

###### Let's witness it

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  fmap =
    \func (IntReader int2a) ->
      IntReader (func . int2a)

instance Applicative IntReader where
  -- Found hole: _pure :: a -> IntReader a
  pure = _pure
  -- Found hole: _apply ::
  --   IntReader (a -> b) -> IntReader a -> IntReader b
  (&lt;*>) = _apply
</code></pre>

---

## The Reader monad

###### Let's witness it

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  fmap =
    \func (IntReader int2a) ->
      IntReader (func . int2a)

instance Applicative IntReader where
  -- Found hole: _pure :: a -> IntReader a
  pure = \a -> IntReader (\_ -> a)
  -- Found hole: _apply ::
  --   IntReader (a -> b) -> IntReader a -> IntReader b
  (&lt;*>) = _apply
</code></pre>

---

## The Reader monad

###### Let's witness it

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  fmap =
    \func (IntReader int2a) ->
      IntReader (func . int2a)

instance Applicative IntReader where
  -- Found hole: _pure :: a -> IntReader a
  pure = \a -> IntReader (\_ -> a)
  -- Found hole: _apply ::
  --   IntReader (a -> b) -> IntReader a -> IntReader b
  (&lt;*>) = 
    \(IntReader f) (IntReader a) ->
      IntReader (\i -> f i (a i))
</code></pre>

---

## The Reader monad

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  fmap =
    \func (IntReader int2a) ->
      IntReader (func . int2a)

instance Applicative IntReader where
  pure = \a -> IntReader (\_ -> a)
  (&lt;*>) = 
    \(IntReader f) (IntReader a) ->
      IntReader (\i -> f i (a i))

instance Monad IntReader where
  -- history
  return = pure
  -- Found hole:
  --   IntReader a -> (a -> IntReader b) -> IntReader b
  (>>=) = _bind
</code></pre>
</div>

---

## The Reader monad

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReader a = IntReader (Int -> a)

runIntReader :: IntReader a -> Int -> a
runIntReader (IntReader f) = f

instance Functor IntReader where
  fmap =
    \func (IntReader int2a) ->
      IntReader (func . int2a)

instance Applicative IntReader where
  pure = \a -> IntReader (\_ -> a)
  (&lt;*>) = 
    \(IntReader f) (IntReader a) ->
      IntReader (\i -> f i (a i))

instance Monad IntReader where
  -- history
  return = pure
  -- Found hole:
  --   IntReader a -> (a -> IntReader b) -> IntReader b
  (>>=) = \(IntReader a) func ->
    IntReader (\i -> runIntReader (func (a i)) i)
</code></pre>
</div>

---

## The Reader monad

###### The use-case for reader

* Suppose an application that is made up of functions
* Many of those functions accept an `Int` argument
* For example, the application configured *port number*

---

## The Reader monad

###### The use-case for reader

* Na&#239;vely we'd pass the `Int` argument through the entire application

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f port (g (h port)) port (j port) (k (l m port))
</code></pre>

---

## The Reader monad

###### The use-case for reader

* Na&#239;vely we'd pass the `Int` argument through the entire application
* This is *clumsy*

<pre><code class="language-haskell hljs" data-trim data-noescape>
application <mark>port</mark> =
  f <mark>port</mark> (g (h <mark>port</mark>)) <mark>port</mark> (j <mark>port</mark>) (k (l m <mark>port</mark>))
</code></pre>

---

## The Reader monad

###### The use-case for reader

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f port (g (h port)) port (j port) (k (l m port))
</code></pre>

* We want to explicitly pass the argument *once*
* Have the reader monad take care of *threading it through our application*

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m port =
  let r =
        do  a1 &lt;- id
            a2 &lt;- g . h
            a3 &lt;- id
            a4 &lt;- j
            a5 &lt;- k . l m
            pure (f a1 a2 a3 a4 a5)
  in  r port
</code></pre>

---

## The Reader monad

###### The use-case for reader

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f port (g (h port)) port (j port) (k (l m port))
</code></pre>

* Or even thread it through explicitly *zero times*
* Using eta-reduction

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m <mark>port</mark> =
  let r =
        do  a1 &lt;- id
            a2 &lt;- g . h
            a3 &lt;- id
            a4 &lt;- j
            a5 &lt;- k . l m
            pure (f a1 a2 a3 a4 a5)
  in  r <mark>port</mark>
</code></pre>

---

## The Reader monad

###### The use-case for reader

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f port (g (h port)) port (j port) (k (l m port))
</code></pre>

* Or even thread it through explicitly *zero times*
* Using eta-reduction

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        do  a1 &lt;- id
            a2 &lt;- g . h
            a3 &lt;- id
            a4 &lt;- j
            a5 &lt;- k . l m
            pure (f a1 a2 a3 a4 a5)
</code></pre>

---

## The Reader monad

###### The use-case for reader

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f <mark>port</mark> (g (h port)) port (j port) (k (l m port))
</code></pre>

* Note the correspondence between the previous code
* and the code without the explicit argument

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        do  a1 &lt;- <mark>id</mark>
            a2 &lt;- g . h
            a3 &lt;- id
            a4 &lt;- j
            a5 &lt;- k . l m
            pure (f a1 a2 a3 a4 a5)
</code></pre>

---

## The Reader monad

###### The use-case for reader

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f port <mark>(g (h port))</mark> port (j port) (k (l m port))
</code></pre>

* Note the correspondence between the previous code
* and the code without the explicit argument

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        do  a1 &lt;- id
            a2 &lt;- <mark>g . h</mark>
            a3 &lt;- id
            a4 &lt;- j
            a5 &lt;- k . l m
            pure (f a1 a2 a3 a4 a5)
</code></pre>

---

## The Reader monad

###### The use-case for reader

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f port (g (h port)) <mark>port</mark> (j port) (k (l m port))
</code></pre>

* Note the correspondence between the previous code
* and the code without the explicit argument

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        do  a1 &lt;- id
            a2 &lt;- g . h
            a3 &lt;- <mark>id</mark>
            a4 &lt;- j
            a5 &lt;- k . l m
            pure (f a1 a2 a3 a4 a5)
</code></pre>

---

## The Reader monad

###### The use-case for reader

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f port (g (h port)) port <mark>(j port)</mark> (k (l m port))
</code></pre>

* Note the correspondence between the previous code
* and the code without the explicit argument

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        do  a1 &lt;- id
            a2 &lt;- g . h
            a3 &lt;- id
            a4 &lt;- <mark>j</mark>
            a5 &lt;- k . l m
            pure (f a1 a2 a3 a4 a5)
</code></pre>

---

## The Reader monad

###### The use-case for reader

<pre><code class="language-haskell hljs" data-trim data-noescape>
application port =
  f port (g (h port)) port (j port) <mark>(k (l m port))</mark>
</code></pre>

* Note the correspondence between the previous code
* and the code without the explicit argument

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        do  a1 &lt;- id
            a2 &lt;- g . h
            a3 &lt;- id
            a4 &lt;- j
            a5 &lt;- <mark>k . l m</mark>
            pure (f a1 a2 a3 a4 a5)
</code></pre>

---

## The Reader monad

###### The use-case for reader

* `(>>=)` allows us to potentially use the values on the left of `<-` on the right of `<-`
* However, in this case, none of the values on the left of `<-` are used on the right of `<-`

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        do  <mark>a1</mark> &lt;- <mark>id</mark>
            <mark>a2</mark> &lt;- <mark>g . h</mark>
            <mark>a3</mark> &lt;- <mark>id</mark>
            <mark>a4</mark> &lt;- <mark>j</mark>
            <mark>a5</mark> &lt;- <mark>k . l m</mark>
            pure (f a1 a2 a3 a4 a5)
</code></pre>

---

## The Reader monad

###### The use-case for reader

* None of the values on the left of `<-` are used on the right of `<-`
* (also required: the last line is a call to `pure`)
* **This exemplifies the difference in power between `(>>=)` and `(<*>)`**

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        do  a1 &lt;- id
            a2 &lt;- g . h
            a3 &lt;- id
            a4 &lt;- j
            a5 &lt;- k . l m
            pure (f a1 a2 a3 a4 a5)
</code></pre>

---

## The Reader monad

###### The use-case for reader

* Since none of the values on the left of `<-` are used on the right of `<-`
* and the last line is a call to `pure`
* This program can be rewritten using only `Applicative`
* The additional power of `(>>=)` is *not required*

<pre><code class="language-haskell hljs" data-trim data-noescape>
application f g h j k l m =
        pure f &lt;*> id &lt;*> g . h &lt;*> id &lt;*> j &lt;*> k . l m
</code></pre>

---

## The Reader monad

###### Exercise

<div style="text-align: center; font-size: 24px">
  Use automated testing to check that all these programs *are the same program*
</div>

