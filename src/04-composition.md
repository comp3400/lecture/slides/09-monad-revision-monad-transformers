## Composition of functors { data-transition="fade none" }

###### For example

* We know that `Optional` is a `Functor`
* We know that `List` is a `Functor`
* But what if we have a `List` of `Optional`?

---

## Composition of functors

###### For example

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ListOptional a = ListOptional (List (Optional a))

instance Functor ListOptional where
  fmap = _ -- will this work?
</code></pre>

---

## Composition of functors

* Let's have another look at the type of `fmap`
* Remembering that `(<$>)` is an alias for `fmap`

<pre><code class="language-haskell hljs" data-trim data-noescape>
> :type fmap
fmap :: Functor k => (a -> b) -> k a -> k b
> :type (&lt;$>)
(&lt;$>) :: Functor k => (a -> b) -> k a -> k b
</code></pre>

---

## Composition of functors

* OK, but what about the type of `\f -> fmap (fmap f)`?

<pre><code class="language-haskell hljs" data-trim data-noescape>
> :type \f -> fmap (fmap f)
\f -> fmap (fmap f)
  :: (Functor j, Functor k) => (a -> b) -> j (k a) -> j (k b)
</code></pre>

---

## Composition of functors

* OK, but what about the type of `\f -> fmap (fmap f)`?
* **It makes yet another `fmap`**

<pre><code class="language-haskell hljs" data-trim data-noescape>
> :type \f -> fmap (fmap f)
\f -> fmap (fmap f)
  :: (Functor j, Functor k) => (a -> b) -> <mark>j (k</mark> a) -> <mark>j (k</mark> b)
</code></pre>

---

## Composition of functors

* OK, but what about the type of `\f -> fmap (fmap f)`?
* If `j` is a `Functor`, and `k` is a `Functor`...
* then the *composition* of `j` and `k` is also a `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
> :type \f -> fmap (fmap f)
\f -> fmap (fmap f)
  :: (Functor j, Functor k) => (a -> b) -> j (k a) -> j (k b)
</code></pre>

---

## Composition of functors

###### For example

* We know that `List` is a `Functor`
* We know that `Optional` is a `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ListOptional a = ListOptional (List (Optional a))

instance Functor ListOptional where
  -- Therefore, we should be able to write this
  -- using only fmap
  -- (and bit of constructor wrapping/unwrapping)
  fmap = _
</code></pre>

---

## Composition of functors

###### For example

* We know that `List` is a `Functor`
* We know that `Optional` is a `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ListOptional a = ListOptional (List (Optional a))

instance Functor ListOptional where
  -- Therefore, we should be able to write this
  -- using only fmap
  -- (and bit of constructor wrapping/unwrapping)
  fmap =
    \f (ListOptional x) ->
      ListOptional (fmap (fmap f) x)
</code></pre>

---

## Composition of functors

###### Functors compose

* The composition of two functors makes another functor
* We say that *functors are closed under composition*
* Or in short, functors compose

---

## Composition of functors

###### Functors compose

* We can generalise this problem to *any two functors*
* Not just `List` or `Optional`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Composition f g a = Composition (f (g a))

-- if f and g are functors
instance (Functor f, Functor g) =>
    -- then the composition is a functor
    Functor (Composition f g) where
  fmap =
    \f (Composition x) ->
      Composition (fmap (fmap f) x)
</code></pre>

---

## Composition of functors

###### What about `Applicative`?

---

## Composition of applicative functors

###### What about `Applicative`?

* Is the composition of two `Applicative` also `Applicative`?
* Are applicative functors *closed under composition*?

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Composition f g a = Composition (f (g a))

instance (Applicative f, Applicative g) =>
    Applicative (Composition f g) where
  pure = _ -- ?
  (&lt;*>) = _ -- ?
</code></pre>

---

## Composition of applicative functors

###### What about `Applicative`?

* Is the composition of two `Applicative` also `Applicative`?
* Are applicative functors *closed under composition*?

<pre style="background-color: black; color: white">
> :type \x -> pure (pure x)
\x -> pure (pure x)
  :: (Applicative f, Applicative g) => a -> f (g a)
> :type \f x -> pure (&lt;*>) &lt;*> f &lt;*> x
\f x -> pure (&lt;*>) &lt;*> f &lt;*> x
  :: (Applicative f, Applicative g) =>
     f (g (a -> b)) -> f (g a) -> f (g b)
</pre>

---

## Composition of applicative functors

###### What about `Applicative`?

* Is the composition of two `Applicative` also `Applicative`?
* Are applicative functors *closed under composition*?

<pre style="background-color: black; color: white">
> :type \x -> pure (pure x)
\x -> pure (pure x)
  :: (Applicative f, Applicative g) => a -> <mark>f (g</mark> a)
> :type \f x -> pure (&lt;*>) &lt;*> f &lt;*> x
\f x -> pure (&lt;*>) &lt;*> f &lt;*> x
  :: (Applicative f, Applicative g) =>
     <mark>f (g</mark> (a -> b)) -> <mark>f (g</mark> a) -> <mark>f (g</mark> b)
</pre>

---

## Composition of applicative functors

###### What about `Applicative`?

* Is the composition of two `Applicative` also `Applicative`?
* Are applicative functors *closed under composition*?

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Composition f g a = Composition (f (g a))

instance (Applicative f, Applicative g) =>
    Applicative (Composition f g) where
  pure = \x -> Composition (pure (pure x))
  (&lt;*>) =
    \(Composition f) (Composition x) ->
      Composition (pure (&lt;*>) &lt;*> f &lt;*> x)
</code></pre>

---

## Composition of applicative functors

###### Applicative functors compose

* The composition of two applicative functors makes another applicative functor
* *applicative functors are closed under composition*

---

## Composition of monads

###### What about `Monads`?

---

## Composition of monads

###### What about `Monad`?

* Is the composition of two `Monad` also `Monad`?
* Are monads *closed under composition*?

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Composition f g a = Composition (f (g a))

instance (Monad f, Monad g) =>
    Monad (Composition f g) where
  return = _ -- ?
  (>>=) = _ -- ?
</code></pre>

---

## Composition of monads

###### What about `Monad`?

* It is **very important** to try this exercise
* Gain an intuition into whether it is possible and why

---

## Composition of monads

###### What about `Monad`?

<div style="text-align: center; font-size: 36px">
  Spoiler Alert
</div>

---

## Composition of monads

###### What about `Monad`?

<div style="text-align: center; font-size: 36px">
  Monads are NOT closed under composition
</div>

---

## Composition of monads

###### What about `Monad`?

* Monads are not closed under composition
* This will be impossible to write
* Please try it to understand why

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Composition f g a = Composition (f (g a))

instance (Monad f, Monad g) =>
    Monad (Composition f g) where
  return = _ -- this will be possible
  (>>=) = _ -- this will not be possible
</code></pre>

---

## Composition of monads

###### Monads are not closed under composition

* This raises an important question
* `List` has `(>>=)`
* `Optional` has `(>>=)`
* If I have a `List` of `Optional`, can I use `(>>=)`?

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ListOptional a = ListOptional (List (Optional a))

-- what if I need this?
-- ListOptional a -> (a -> ListOptional b) -> ListOptional b
</code></pre>

---

## Composition of monads

###### Monads are not closed under composition

* Although monads are not closed under composition
* We can still write `(>>=)` if the *outer monad* is also a monad

---

## Composition of monads

###### Monads are not closed under composition

* The outer monad in `ListOptional` is `List`
* We can generalise it to a type variable

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ListOptional a = ListOptional (<mark>List</mark> (Optional a))
data OptionalT k a = OptionalT (<mark>k</mark> (Optional a))
</code></pre>

---

## Composition of monads

###### Monads are not closed under composition

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ListOptional a = ListOptional (List (Optional a))
data OptionalT k a = OptionalT (k (Optional a))

-- exercises, try it!
instance Functor k => Functor (OptionalT k) where
instance Applicative k => Applicative (OptionalT k) where
instance Monad k => Monad (OptionalT k) where
</code></pre>

---

## Composition of monads

###### Monads are not closed under composition

* This data type is called a **monad transformer**
* Specifically, the `Optional` monad transformer
* It combines any arbitrary outer monad `(k)` with `Optional`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data OptionalT k a = OptionalT (k (Optional a))
</code></pre>

---

## Composition of monads

###### The `Identity` monad

* `Identity` is a container wrapping exactly one value
* The behaviour of `Identity anything`
* is the same as the behaviour of `anything`
* `Identity` is a `Monad`
* `Identity` is quite boring...

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Identity a = Identity a

instance Functor Identity where
  fmap f (Identity a) = Identity (f a)
instance Applicative Identity where
  pure = Identity
  Identity f &lt;*> Identity a =
    Identity (f a)
instance Monad Identity where
  return = pure
  Identity x >>= f =
    f x
</code></pre>

---

## Composition of monads

###### However, the original monad can be recovered

* We can get the behaviour of `Optional` from `OptionalT`
* We'd set `(k)` to be `Identity`
* By doing this, we are combining a *neutral* monad with `Optional`
* That is, we now have a data type that behaves just like `Optional`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data OptionalT k a = OptionalT (k (Optional a))

data Identity a = Identity a
type <mark>OptionalAgain</mark> a = OptionalT Identity a
</code></pre>

---

## Monad Transformers

<div style="text-align: center; font-size: 36px">
  Monad transformers exist because monads are not closed under composition
</div>

