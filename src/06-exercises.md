## Exercises { data-transition="fade none" }

* Examine through the working example of `State`
* Try a different use-case
* Trace out what is happening in `(>>=)` and `pure`
* Observe why the `Monad` instance of `State s` solves `add10`

---

## Exercises

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- Given data Identity
data Identity a = Identity a deriving (Eq, Show)
</code></pre>

* Write the `Functor`, `Applicative` and `Monad` instances
* Using `traverse`
  * Write a function :: `Monad k => (a -> b) -> k a -> k b`
  * You will use `Identity` to write this function
  * What function have we already seen resembles this?

---

## Exercises

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- Given data ParseResult and data Parser
data ParseResult a =
  ParseError String
  | ParseSuccess a Input

data Parser a = Parser (Input -> ParseResult a)
</code></pre>

* Write the `Functor` instance for `ParseResult`
* Write the `Functor`, `Applicative` and `Monad` instances for `Parser`
* Examine the relationship between `State` and `Parser`
  * What is the *state* being used?
  * Can you rewrite the data types so that you can just reuse `State`?

---

## Exercises

###### `State` has a monad transformer

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- Given data StateT
data StateT s k a =
  StateT (s -> k (s, a))
</code></pre>

* Write the `Functor`, `Applicative` and `Monad` instances for `StateT s k`
* Recover the `State` data type using `Identity` and `StateT`

<pre><code class="language-haskell hljs" data-trim data-noescape>
instance Functor k =>
  Functor (StateT s k) where -- todo

instance Monad k => -- why does this have to be Monad?
  Applicative (StateT s k) where -- todo

instance Monad k =>
  Monad (StateT s k) where -- todo
</code></pre>

---

## Exercises

* Read the paper, *The Essence of the Iterator Pattern* on blackboard for the original insight into `traverse`
